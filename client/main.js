(function() {
  'use strict';

  const socket = io();

  socket.on('update', data => {
    document.querySelector('#last-updated-time').innerHTML = `MTA last update: ${data.mtaTimestamp}, Server last update: ${data.serverTimestamp}`;

    document.querySelector('#status-123').innerHTML = data.lines123.status;
    document.querySelector('#text-123').innerHTML = data.lines123.text;
    document.querySelector('#time-123').innerHTML = !!data.lines123.time ? `As of ${data.lines123.time}` : '';

    document.querySelector('#status-456').innerHTML = data.lines456.status;
    document.querySelector('#text-456').innerHTML = data.lines456.text;
    document.querySelector('#time-456').innerHTML = !!data.lines456.time ? `As of ${data.lines456.time}` : '';

    document.querySelector('#status-7').innerHTML = data.line7.status;
    document.querySelector('#text-7').innerHTML = data.line7.text;
    document.querySelector('#time-7').innerHTML = !!data.line7.time ? `As of ${data.line7.time}` : '';

    document.querySelector('#status-ACE').innerHTML = data.linesACE.status;
    document.querySelector('#text-ACE').innerHTML = data.linesACE.text;
    document.querySelector('#time-ACE').innerHTML = !!data.linesACE.time ? `As of ${data.linesACE.time}` : '';

    document.querySelector('#status-BDFM').innerHTML = data.linesBDFM.status;
    document.querySelector('#text-BDFM').innerHTML = data.linesBDFM.text;
    document.querySelector('#time-BDFM').innerHTML = !!data.linesBDFM.time ? `As of ${data.linesBDFM.time}` : '';

    document.querySelector('#status-G').innerHTML = data.lineG.status;
    document.querySelector('#text-G').innerHTML = data.lineG.text;
    document.querySelector('#time-G').innerHTML = !!data.lineG.time ? `As of ${data.lineG.time}` : '';

    document.querySelector('#status-JZ').innerHTML = data.linesJZ.status;
    document.querySelector('#text-JZ').innerHTML = data.linesJZ.text;
    document.querySelector('#time-JZ').innerHTML = !!data.linesJZ.time ? `As of ${data.linesJZ.time}` : '';

    document.querySelector('#status-L').innerHTML = data.lineL.status;
    document.querySelector('#text-L').innerHTML = data.lineL.text;
    document.querySelector('#time-L').innerHTML = !!data.lineL.time ? `As of ${data.lineL.time}` : '';

    document.querySelector('#status-NQRW').innerHTML = data.linesNQRW.status;
    document.querySelector('#text-NQRW').innerHTML = data.linesNQRW.text;
    document.querySelector('#time-NQRW').innerHTML = !!data.linesNQRW.time ? `As of ${data.linesNQRW.time}` : '';

    const statusTags = document.querySelectorAll('.status-tag');
    for (let tag of statusTags) {
      if (tag.innerHTML === 'GOOD SERVICE') {
        tag.classList.remove('red');
        tag.classList.add('green');
      } else {
        tag.classList.remove('green');
        tag.classList.add('red');
      }
    }
  });

  const statusTags = document.querySelectorAll('.status-tag');
  for (let tag of statusTags) {
    tag.onclick = () => {
      const expand = tag.parentNode.getElementsByClassName('expand')[0];
      const isShown = expand.style.display;
      expand.style.display = isShown === '' ? 'initial' :
                isShown === 'initial' ? 'none' : 'initial';
    };
  }
})();
