'use strict';

/**
 * Module dependencies.
 * @private
 */
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const xml2js = require('xml2js');
const config = require('./config');


const app = express();
app.use(express.static(`${__dirname}/client`));

const httpServer = http.Server(app);
httpServer.listen(config.PORT, () => {
  console.log(`Listening on port ${config.PORT}`);
});
const io = socketio.listen(httpServer);

function getHTML(opts) {
  return new Promise(resolve => {
    http.get(opts, res => {
      let html = '';
      res.on('data', data => {
        html += data.toString();
      });
      res.on('end', () => {
        resolve(html);
      });
    });
  });
}

function loadXML(xmlStr) {
  return new Promise(resolve => {
    xml2js.parseString(xmlStr, (err, resObj) => {
      resolve(resObj);
    });
  });
}

async function loadStatus() {
  const xmlStr = await getHTML(config.MTA_SERVICE_OPT);
  const xmlAsObj = await loadXML(xmlStr);

  const metaInfo = xmlAsObj.service;
  const subwayInfo = metaInfo.subway[0].line;

  const getSubwayLineStatus = index => subwayInfo[index].status[0];
  const getSubwayLineText = index => subwayInfo[index].text[0];
  const getSubwayLineTime = index => `${subwayInfo[index].Date[0]}${subwayInfo[index].Time[0]}`;

  const statusCode = metaInfo.responsecode[0];

  return {
    serverTimestamp: Date.now(),
    mtaTimestamp: metaInfo.timestamp[0],
    lines123: {
      status: getSubwayLineStatus(0),
      text: getSubwayLineText(0),
      time: getSubwayLineTime(0),
    },
    lines456: {
      status: getSubwayLineStatus(1),
      text: getSubwayLineText(1),
      time: getSubwayLineTime(1),
    },
    line7: {
      status: getSubwayLineStatus(2),
      text: getSubwayLineText(2),
      time: getSubwayLineTime(2),
    },
    linesACE: {
      status: getSubwayLineStatus(3),
      text: getSubwayLineText(3),
      time: getSubwayLineTime(3),
    },
    linesBDFM: {
      status: getSubwayLineStatus(4),
      text: getSubwayLineText(4),
      time: getSubwayLineTime(4),
    },
    lineG: {
      status: getSubwayLineStatus(5),
      text: getSubwayLineText(5),
      time: getSubwayLineTime(5),
    },
    linesJZ: {
      status: getSubwayLineStatus(6),
      text: getSubwayLineText(6),
      time: getSubwayLineTime(6),
    },
    lineL: {
      status: getSubwayLineStatus(7),
      text: getSubwayLineText(7),
      time: getSubwayLineTime(7),
    },
    linesNQRW: {
      status: getSubwayLineStatus(8),
      text: getSubwayLineText(8),
      time: getSubwayLineTime(8),
    },
  };
}

async function emitStatusNonstop() {
  const status = await loadStatus();
  console.log(status.serverTimestamp, status.mtaTimestamp);
  io.emit('update', status);
  emitStatusNonstop();
}

emitStatusNonstop();
